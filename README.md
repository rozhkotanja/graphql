﻿
# running every **Artisan** command inside the **php-fpm bash** to avoid compatibility issues

    docker-compose exec php-fpm bash
# kill docker proccess

    docker-compose kill
# build docker

    docker-compose build
# run docker 

    docker-compose up

# see all containers

     docker ps

[download docker for windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)

Available only with Linux containers! If you use Windows just switch to Linux containers

# build anugular image 

docker image build -t angular .

Server [http://localhost:8081/](http://localhost:8081/)
Front-end [http://localhost:3000/](http://localhost:3000/)